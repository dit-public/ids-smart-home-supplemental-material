import pandas as pd
import datetime as dt
from pymongo import MongoClient

SCALE = 60 / 200e-3  # the pcap scale proportion (REAL_SECONDS / SIM_SECONDS)
BASE_EPOCH_NS = 1333263600000000000  # first epoch in .csv files
BASE_EPOCH_SIM_NS = None  # set as none to programmatically find this value

MONTHLY_STORAGE = True

FILTER = {"nDPIclass": "MQTT", "dir": "A"}

client = MongoClient("mongodb://localhost:27017/")
db_src = client["tranalyzer"]
db_dst = client["smarthome"]


def batched(cursor, batch_size):
    batch = []
    for doc in cursor:
        batch.append(doc)
        if batch and not len(batch) % batch_size:
            yield batch
            batch = []

    if batch:  # last documents
        yield batch


if BASE_EPOCH_SIM_NS is None:
    cursor = db_src["flow"].find(FILTER)
    for batch in batched(cursor, 50000):
        df_flow = pd.DataFrame(list(batch))
        new_min = min(df_flow["timeFirst"].astype("int64"))
        BASE_EPOCH_SIM_NS = (
            new_min
            if BASE_EPOCH_SIM_NS is None or new_min < BASE_EPOCH_SIM_NS
            else BASE_EPOCH_SIM_NS
        )

print(f"First timestamp: {dt.datetime.fromtimestamp(BASE_EPOCH_SIM_NS // 1000000000)}")

cursor = db_src["flow"].find(FILTER)

for batch in batched(cursor, 50000):
    df_flow = pd.DataFrame(list(batch))

    epoch = df_flow["timeFirst"].astype("int64") - BASE_EPOCH_SIM_NS
    epoch_scaled = SCALE * epoch + BASE_EPOCH_NS
    date_scaled = pd.to_datetime(epoch_scaled, unit="ns")
    month_scaled = date_scaled.dt.strftime("%Y-%m")

    unique_months = month_scaled.unique().tolist()

    df_flow["timeFirst"] = date_scaled
    df_flow["timeLast"] = pd.to_datetime(
        date_scaled.astype("int64") + df_flow["duration"].astype("int64"), unit="ns"
    )

    if True == MONTHLY_STORAGE:
        for m in unique_months:
            db_dst[f"smarthome_{m}"].insert_many(
                df_flow[month_scaled == m].sort_values(by="flowInd").to_dict("records")
            )
    else:
        db_dst["smarthome"].insert_many(
            df_flow.sort_values(by="flowInd").to_dict("records")
        )

    print(f'Last processed timestamp: {df_flow["timeFirst"].max()}')
