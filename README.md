# IDS Smart Home Supplemental Material

## Prerrequisites

- A running `mongodb` instance
- `Docker` and `docker-compose`
- `tranalyzer`

## Project structure

- **00_dataset:** data public dataset in csv format that includes the electrical consumption of 30 smart-home devices over 13 months, from:
  - Makonin, S., Popowich, F., Bartram, L., Gill, B., & Bajić, I. V. (2013, August). AMPds: A public dataset for load disaggregation and eco-feedback research. In 2013 IEEE electrical power & energy conference (pp. 1-6). IEEE.
- **01_pcap-generation:** tools to generate network traffic from .csv files found in `01_pcap-generation/input`. To accelerate the simulation, we apply a scale defined in `01_pcap-generation/mqtt_publisher.py`. For example, SCALE=0.1 represents that 1 minute of traffic is simulated in 0.1 seconds.
- **02_iot-device-pcap:** network traffic generated in pcap format for first 6 months, generated using `01_pcap-generation` tool with the default scale.
- **03_flow-rescaling:** tool that rescales network flows. Flows have to be previously generated with `tranalyzer` and dumped into `mongodb`. The scale can be defined in `03_flow-rescaling/rescale.py` (by default 0.1).
- **04_iot-device-flows:** rescaled network flows for first 3 months.
- **05_attack-pcap:** network attacks traffic in pcap format.

## How to run:

1. To generate network traffic from public iot dataset:
   - Copy dataset files (`00_dataset`) to `01_pcap-generation/input` directory.
   - Run docker with `docker compose up -d`
     - (Optional) If something changed (To rebuild): `docker-compose build`
2. To generate network flows:
   - Run tranalyzer `tranalyzer -r <pcap-file>`
   - Tranalyzer exports the flows in a mongodb collection called flows.
   - Tranalyzer version: tranalyzer2-0.8.13. Available at https://tranalyzer.com/
   - We used the following Tranalyzer modules:
     - basicFlow
     - macRecorder
     - nDPI
     - geoip
     - tp0f
     - basicStats
     - tcpFlags
     - arpDecode
     - sslDecode
     - p0f
     - mongoSink
3. Rescale network flows by running `03_flow-rescaling/rescale.py`.
4. Prepare attacks pcap by merging provided attacks (`05_attack-pcap`) into the flows.
   - Option 1: generate attacks flows and merge them into the dataset flows manually (editing mongodb records)
   - Option 2: use editcap and mergecap to insert them into the pcaps, then recreate the flows
