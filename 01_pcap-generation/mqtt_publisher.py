import subprocess
import sys

# import os
import time
from datetime import datetime

import pandas as pd
import pause

SCALE = 0.100  # in seconds

broker = sys.argv[1]
topic = sys.argv[2]
df = pd.read_csv("./input/{}.csv".format(topic))
total_rows = df.__len__()
i = 0
ts_1 = 0
st = 0

# The following line allows the simulation to start at a given datetime (to sync)
# pause.until(datetime(2022, 6, 7, 12, 20, 00))  # in isotime

for data in df.itertuples():
    try:
        # st = time.time()
        i += 1
        data = data._asdict()
        if data["TS"] >= 1349074800:  # up to 1 Oct in epoch time
            break
        sleep_time = (data["TS"] - ts_1) / 60 if i != 1 else 0
        ts_1 = data["TS"]
        et = time.time()
        time.sleep(
            (SCALE * sleep_time - (et - st))
            if (i != 1) and (SCALE * sleep_time - (et - st)) > 0
            else 0
        )
        # os.system("mosquitto_pub -h {} -t {} -m '{}'".format(broker, topic, data))
        st = time.time()
        subprocess.Popen(
            ["mosquitto_pub", "-h", broker, "-t", topic, "-m", f"'{data}'"]
        )
    except Exception:
        continue

print("Finish simulation")
