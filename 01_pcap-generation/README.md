# Smart Home 

## How to:

To run (and build if first time):
``` bash
docker compose up -d
```

To rebuild (if something changed):
``` bash
docker-compose build
```